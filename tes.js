class Teacher {

  constructor(name) {

    this._name = name

    this._students = []

  }

  addStudent(student) {

    this._students.push(student)

  }

  get students() {

    return this._students

  }

}

class Student {

  constructor(name) {

    this._name = name

    this._teachers = []

  }

  chooseTeachers(teacher) {

    this._teachers.push(teacher)

  }

  get teachers() {

    return this._teachers

  }

}

// Masing masing class dibuat object nya sendiri-sendiri

const abduh = new Teacher("abduh")

const regi = new Student("Regi")

// suatu object dapat memiliki object class lainnya 

abduh.addStudent(regi)

console.log(abduh.students)

regi.chooseTeachers(abduh)

console.log(regi.teachers)