export class Bootcamp {
    constructor(name){
        this._name = name
        this._classes = []
        this._students =[]
        this._level = []
        this._instructor = []
        
    }
    get name(){
        return this._name 
    }
    
    get classes(){
        return this._classes
        
    }
    set classes(namakelas){
        return namakelas = this._classes
    }
    createClass(kelas,level,instructor){
        this._classes.push(kelas)
        this._level.push(level)
        this._instructor.push(instructor)
      
    
    }
   
   
  
}
